package servicios;

import java.io.IOException;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.Endpoint;

import uytube.UsuarioController.UsuarioController;
import uytube.models.Canal;
import uytube.App;
import uytube.CanalController.CanalController;

@WebService
@SOAPBinding(style = Style.RPC, parameterStyle = ParameterStyle.WRAPPED)
public class CanalEndpoint {

	private Endpoint endpoint;
	private UsuarioController controllerUser;
	private CanalController controllerCanal;
	

	public CanalEndpoint() {
		
		// TODO Auto-generated constructor stub
		controllerUser = new UsuarioController();
		controllerCanal = new CanalController();
	}

	@WebMethod(exclude = true)
	public void publicar() {
		String ip = "localhost"; //default
//		try {
//			ip = App.LecuturaIP();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		String ipTotal = "http://"+ip+":9880/canal";
		System.out.println("LA IP TOTAL ES: "+ipTotal);
		endpoint = Endpoint.publish(ipTotal, this);
	}

	@WebMethod(exclude = true)
	public Endpoint getEndpoint() {
		return endpoint;
	}

	// CanalesSeguidos
	@WebMethod
	public Canal[] CanalesSeguidos(String nick) {
		List<Canal> canales = this.controllerUser.listCanalesSeguidos(nick);
		Canal[] retornar = new Canal[canales.size()];
		for (int i = 0; i < retornar.length; i++) {
			retornar[i] = canales.get(i);
		}
		return retornar;
	}
	
	@WebMethod
	public Canal obtenerCanalUsuario(String nickname) {
		return this.controllerCanal.obtenerCanalUsuario(nickname);
	}
	
	@WebMethod
	public void actualizarCanal(Canal canal) {
		this.controllerCanal.actualizarCanal(canal);
	}
	
	@WebMethod
	public void darDeBaja(String nickname) {
		this.controllerUser.darDeBaja(nickname);
		System.out.println("El usuario "+nickname+" fue dado de baja");
	}
}
