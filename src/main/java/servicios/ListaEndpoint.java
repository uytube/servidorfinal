package servicios;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.Endpoint;

import uytube.App;
import uytube.ListaController.ListaController;
import uytube.models.Lista;
import uytube.models.Video;

@WebService
@SOAPBinding(style = Style.RPC, parameterStyle = ParameterStyle.WRAPPED)
public class ListaEndpoint {

	private Endpoint endpoint;
	private ListaController controllerLista;
	
	public ListaEndpoint() {
		controllerLista = new ListaController();

	}
	
	@WebMethod(exclude = true)
	public void publicar() {
		String ip = "localhost"; //default
//		try {
//			ip = App.LecuturaIP();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
		String ipTotal = "http://"+ip+":9877/lista";
		System.out.println("LA IP TOTAL ES: "+ipTotal);
		endpoint = Endpoint.publish(ipTotal, this);
	}

	@WebMethod(exclude = true)
	public Endpoint getEndpoint() {
		return endpoint;
	}
	
	//Listar listas del usuario logueado
	@WebMethod
	public Lista[] listarListas(String nick) {
		List<Lista> listas = this.controllerLista.listarListas(nick);
		Lista[] retornar = new Lista[listas.size()];
		for (int i = 0; i < retornar.length; i++) {
			retornar[i] = listas.get(i);
		}
		return retornar;
	}
	
	//Crear lista de repro.
	@WebMethod
	public void postCrearLista(String nombre_lista,String categoria,boolean privado,String usuario) {
		this.controllerLista.crearLista(nombre_lista, "Sin Categoria",usuario, privado, false);	
	}
	
	//Quitar video de la lsita de reproduccion
	@WebMethod
	public String quitarVideo(int id_video, int id_lista) {

		try {
			this.controllerLista.quitarVideo(id_video, id_lista);
			return "Video quitado de la lista";
		} catch (Exception e) {
			return "Video no esta en la lista";
		}
	}
	
	//Agregar video a la lista de reproduccion
	@WebMethod
	public String agregarVideo(int id_video, int id_lista) {

		try {
			this.controllerLista.agregarVideo(id_video, id_lista);
			return "Video agregado a la lista";
		} catch (Exception e) {
			return "Video ya esta en la lista";
		}
	}
	
	@WebMethod
	public Lista listaPorID(int idList) {
		Lista retorno = new Lista();
		retorno = this.controllerLista.obtenerListaPorId(idList);
		System.out.println("IM :"+retorno.getNombre());
		return retorno;
	}
	
	@WebMethod
	public Video[] videosListaReproduccion(int idList) {
		Lista listas =  this.controllerLista.obtenerListaPorId(idList);
		List<Video> videosL = listas.getVideos();
		Video[] retornar = new Video[videosL.size()];
		for (int i = 0; i < retornar.length; i++) {
			retornar[i] = videosL.get(i);
		}
		return retornar;
	}
	
	
	//Modificar Lista - privacidad
	@WebMethod
	public void modificarLista(int idLista,boolean privacidad) {
		this.controllerLista.modificarLista(idLista, this.controllerLista.obtenerListaPorId(idLista).getCategoria().getId(), privacidad);
	}
}
