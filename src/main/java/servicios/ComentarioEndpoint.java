package servicios;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.Endpoint;

import uytube.App;
import uytube.ComentarioController.ComentarioController;
import uytube.UsuarioController.UsuarioController;
import uytube.VideoController.VideoController;
import uytube.models.Comentario;
import uytube.models.Usuario;
import uytube.models.Video;

@WebService
@SOAPBinding(style = Style.RPC, parameterStyle = ParameterStyle.WRAPPED)
public class ComentarioEndpoint {

	private Endpoint endpoint;
	private ComentarioController controllerComentario;
	private UsuarioController controllerUser;
	private VideoController controllerVideo;
	
	public ComentarioEndpoint() {
		
		controllerComentario = new ComentarioController();
		controllerVideo = new VideoController();
		controllerUser = new UsuarioController();
	}
	
	@WebMethod(exclude = true)
	public void publicar() {
		String ip = "localhost"; //default
//		try {
		//ip = App.LecuturaIP();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		String ipTotal = "http://"+ip+":9878/comentario";
		System.out.println("LA IP TOTAL ES: "+ipTotal);
		endpoint = Endpoint.publish(ipTotal, this);
	}

	@WebMethod(exclude = true)
	public Endpoint getEndpoint() {
		return endpoint;
	}
	
	// Comentarios de un video
	@WebMethod
	public Comentario[] ComentariosDeUnVideo(String nombre_video) {
		List<Comentario> comentarios = this.controllerComentario.listarComentarios(nombre_video);
		Comentario[] retornar = new Comentario[comentarios.size()];
		for (int i = 0; i < retornar.length; i++) {
			retornar[i] = comentarios.get(i);
		}
		return retornar;
		// return (ArrayList)this.controllerComentario.listarComentarios(nombre_video);
	}
	
	// Comentar Video o comentario
	@WebMethod
	public String postComentarioVideo(int id_video, String comentario, int id_comentario, String user) {

		Usuario usuario = this.controllerUser.consultarUsuario(user);

		// Si es respuesta
		if (id_comentario != -1) {
			Comentario respuesta = new Comentario();

			respuesta.setComentario(comentario);
			respuesta.setFecha(new Date());
			respuesta.setVid(null);

			respuesta.setUsuario(usuario);

			controllerComentario.AgregarRespuesta((long) id_comentario, respuesta);
			return "WS: Respuesta lista";

		} else {
			// Si es comentario al video

			Video vid = controllerVideo.consultaVideoPorID(id_video);
			Comentario com = new Comentario();

			com.setComentario(comentario);
			com.setFecha(new Date());
			com.setVid(vid);

			com.setUsuario(usuario);

			controllerComentario.AgregarComentario(com);

			return "WS: Comentario listo";
		}
	}
}
