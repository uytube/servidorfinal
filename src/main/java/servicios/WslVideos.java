package servicios;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.Endpoint;

import uytube.models.Categoria;
import uytube.models.Video;
import uytube.App;
import uytube.VideoController.VideoController;;

@WebService
@SOAPBinding(style = Style.DOCUMENT, parameterStyle = ParameterStyle.WRAPPED)
public class WslVideos {

	private Endpoint endpoint;
	private VideoController controller;
	public WslVideos() {
		controller = new VideoController();
	}
	
	@WebMethod(exclude = true)
	public void publicar() {
		String ip = "localhost"; //default
//		try {
//			ip = App.LecuturaIP();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		String ipTotal = "http://"+ip+":9874/videos";
		System.out.println("LA IP TOTAL ES: "+ipTotal);
		endpoint = Endpoint.publish(ipTotal, this);
	}
	
	@WebMethod(exclude = true)
	public Endpoint getEndpoint() {
		return endpoint;
	}
	
	/*
	 * Ejemplo devolviendo un objeto
	 * */
	
	@WebMethod
	public List<Video> listaVideosUsuario(String nickname) {
		List<Video> videos = controller.listaVideosUsuario(nickname);
		return videos;
	}
	@WebMethod
	public List<Video> videoPorCategoria(Categoria categoria) {
		List<Video> videos = controller.videoPorCategoria(categoria);
		return videos;
	}
	@WebMethod
	public List<Video> searchVideo(String search, String fecha, String alfabeticamente) {
		List<Video> videos = controller.searchVideo(search, fecha, alfabeticamente);
		return videos;
	}
	@WebMethod
	public List<Video> videosRandom() {
		List<Video> videos = controller.videosRandom();
		return videos;
	}
	
}