package servicios;

import java.io.IOException;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.Endpoint;

import uytube.App;
import uytube.UsuarioController.UsuarioController;
import uytube.ValoracionController.ValoracionController;
import uytube.VideoController.VideoController;
import uytube.models.Usuario;
import uytube.models.ValoracionVideo;

@WebService
@SOAPBinding(style = Style.RPC, parameterStyle = ParameterStyle.WRAPPED)
public class ValoracionEndpoint {

	private Endpoint endpoint;
	private ValoracionController controllerValoracion;
	private UsuarioController controllerUser;
	private VideoController controllerVideo;


	public ValoracionEndpoint() {
		
		controllerValoracion = new ValoracionController();
		controllerUser = new UsuarioController();
		controllerVideo = new VideoController();
	}

	@WebMethod(exclude = true)
	public void publicar() {
		String ip = "localhost"; //default
//		try {
//			ip = App.LecuturaIP();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		String ipTotal = "http://"+ip+":9879/valoracion";
		System.out.println("LA IP TOTAL ES: "+ipTotal);
		endpoint = Endpoint.publish(ipTotal, this);
	}

	@WebMethod(exclude = true)
	public Endpoint getEndpoint() {
		return endpoint;
	}

	// ValoracionesVideo
	@WebMethod
	public ValoracionVideo[] ValoracionesVideo(int id_video) {
		List<ValoracionVideo> valoraciones = this.controllerValoracion.listaValoracionesVideo(id_video);
		ValoracionVideo[] retornar = new ValoracionVideo[valoraciones.size()];
		for (int i = 0; i < retornar.length; i++) {
			retornar[i] = valoraciones.get(i);
		}

		return retornar;
	}

	// TraerValoracion
	@WebMethod
	public ValoracionVideo DevolverValoracionVideo(int id_video, String nick) {
		return this.controllerValoracion.traerValoracion(id_video, nick);
	}

	// Lista de valoraciones de un video
	@WebMethod
	public ValoracionVideo[] ValoracionesDeVideo(int id) {
		List<ValoracionVideo> valoraciones = this.controllerValoracion.listaValoracionesVideo(id);
		ValoracionVideo[] retornar = new ValoracionVideo[valoraciones.size()];
		for (int i = 0; i < retornar.length; i++) {
			retornar[i] = valoraciones.get(i);
		}

		return retornar;
		// return (ArrayList)this.controllerValoracion.listaValoracionesVideo(id);
	}
	
	// Valorar Video
	@WebMethod
	public String postValorarVideo(int id_video, String user, int nueva_valoracion) {

		ValoracionVideo valoracion;
		try {
			valoracion = controllerValoracion.traerValoracion(id_video, user);
			// En este punto, si el usuairo no habia valorado antes el video, saltara una
			// excepcion
			valoracion.setValoracion(nueva_valoracion);

			System.out.println("Usuario de valoracion: " + valoracion.getUsuario().getNickname());

			controllerValoracion.valorarVideo(valoracion);

			return "WS: Valoracion modificada";
		} catch (Exception e) {
			System.out.println("Except valoration!");
			// Nueva valoracion
			Usuario usuario = controllerUser.consultarUsuario(user);
			valoracion = new ValoracionVideo();
			valoracion.setVideo(controllerVideo.consultaVideoPorID(id_video));
			valoracion.setUsuario(usuario);
			valoracion.setValoracion(nueva_valoracion);
			controllerValoracion.valorarVideo(valoracion);

			return "WS: Nueva valoracion";
		}

	}

}
