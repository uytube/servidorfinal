package servicios;

import java.io.IOException;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.Endpoint;

import uytube.App;
import uytube.CategoriaController.CategoriaController;
import uytube.models.Categoria;

@WebService
@SOAPBinding(style = Style.RPC, parameterStyle = ParameterStyle.WRAPPED)
public class CategoriaEndpoint {
	
	private Endpoint endpoint;
	private CategoriaController controllerCategoria;

	public CategoriaEndpoint() {
		controllerCategoria = new CategoriaController();
	}
	
	// Lista de categorias
	@WebMethod
	public Categoria[] ListaDeCategorias() {
		List<Categoria> categorias = this.controllerCategoria.listarCategorias();
		Categoria[] retornar = new Categoria[categorias.size()];
		for (int i = 0; i < categorias.size(); i++) {
			retornar[i] = categorias.get(i);
		}

		return retornar;
	}
	

	@WebMethod(exclude = true)
	public void publicar() {
		String ip = "localhost"; //default
//		try {
//			ip = App.LecuturaIP();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		String ipTotal = "http://"+ip+":9879/categoria";
		System.out.println("LA IP TOTAL ES: "+ipTotal);
		endpoint = Endpoint.publish(ipTotal, this);
	}

	@WebMethod(exclude = true)
	public Endpoint getEndpoint() {
		return endpoint;
	}
	
	@WebMethod
	public Categoria obtenerCategoria(String categoria) {
		return this.controllerCategoria.obtenerCategoria(categoria);
	}
	
	@WebMethod
	public Categoria obtenerCategoriaPorID(int idcategoria) {
		return this.controllerCategoria.obtenerCategoriaPorId(idcategoria);
	}
	

}
