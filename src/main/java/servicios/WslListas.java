package servicios;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.Endpoint;

import uytube.models.Canal;
import uytube.models.Lista;
import uytube.App;
import uytube.ListaController.ListaController;

@WebService
@SOAPBinding(style = Style.DOCUMENT, parameterStyle = ParameterStyle.WRAPPED)
public class WslListas {

	private Endpoint endpoint;
	private ListaController controller;
	public WslListas() {
		controller = new ListaController();
	} 
	
	@WebMethod(exclude = true)
	public void publicar() {
		String ip = "localhost"; //default
//		try {
//			ip = App.LecuturaIP();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		String ipTotal = "http://"+ip+":9874/listas";
		System.out.println("LA IP TOTAL ES: "+ipTotal);
		endpoint = Endpoint.publish(ipTotal, this);
	}
	
	@WebMethod(exclude = true)
	public Endpoint getEndpoint() {
		return endpoint;
	}
	
	/*
	 * Ejemplo devolviendo un objeto
	 * */
	
	
	@WebMethod
	public Lista[] listarListas(String nickname) {
		List<Lista> listas = controller.listarListas(nickname);
		Lista[] retornar = new Lista[listas.size()];
		for (int i = 0; i < retornar.length; i++) {
			retornar[i] = listas.get(i);
		}
		return retornar;
	}
	@WebMethod
	public List<Lista> searchListas(String search,String alfabeticamente) {
		List<Lista> listas = controller.searchListas(search, alfabeticamente);
		return listas;
	}
	
}