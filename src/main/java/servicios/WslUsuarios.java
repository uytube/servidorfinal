package servicios;

import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.Endpoint;
import org.apache.commons.io.FileUtils;

import uytube.models.Canal;
import uytube.models.Historial;
import uytube.models.Usuario;
import uytube.models.Video;
import uytube.UsuarioController.UsuarioController;
import uytube.VideoController.*;

@WebService
@SOAPBinding(style = Style.DOCUMENT, parameterStyle = ParameterStyle.WRAPPED)
public class WslUsuarios {

	private Endpoint endpoint;
	private UsuarioController controller;
	public WslUsuarios() {
		controller = new UsuarioController();
	}
	
	@WebMethod(exclude = true)
	public void publicar() {
		String ip = "localhost"; //default
//		try {
//			ip = App.LecuturaIP();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		String ipTotal = "http://"+ip+":9874/usuarios";
		System.out.println("LA IP TOTAL ES: "+ipTotal);
		endpoint = Endpoint.publish(ipTotal, this);
	}
	
	@WebMethod(exclude = true)
	public Endpoint getEndpoint() {
		return endpoint;
	}
	
	/*
	 * Ejemplo devolviendo un objeto
	 * */
	
	@WebMethod
	public void crearUsuario(Usuario user, Canal canal, byte[] img, String fileName) {
		if(img!=null) {
		File folder = new File("resources" + File.separator + fileName);
		try {
			FileUtils.writeByteArrayToFile(new File(folder.getAbsolutePath()), img);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		controller.crearUsuario(user, canal);
	}
	public void modificarUsuario(Usuario user, byte[] img, String fileName) {
		if(img!=null) {
		File folder = new File("resources" + File.separator + fileName);
		try {
			FileUtils.writeByteArrayToFile(new File(folder.getAbsolutePath()), img);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		controller.modificarUsuario(user);
	}
	
	@WebMethod
	public Usuario consultarUsuario(String nickname) {
		Usuario user = controller.consultarUsuario(nickname);	
		byte[] fileContent = null;
		File folder = new File("resources");
		File file = new File(folder.getAbsolutePath()+ File.separator+user.getImg());
		System.out.println(file.exists());
		System.out.println(file.getAbsolutePath());
		if(file.exists() && !file.isDirectory()) {
			try {
				
				fileContent = FileUtils.readFileToByteArray(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
			String encodedString = Base64.getEncoder().encodeToString(fileContent);
			user.setImg(encodedString);
		}
		return user;
	}
	@WebMethod
	public Usuario login(String nickname, String password) {
		byte[] fileContent = null;
		Usuario user = controller.login(nickname, password);
		File folder = new File("resources");
		if(user!=null) {
		File file = new File(folder.getAbsolutePath()+ File.separator+user.getImg());
		if(file.exists() && !file.isDirectory()) {
			try {
				
				fileContent = FileUtils.readFileToByteArray(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
			String encodedString = Base64.getEncoder().encodeToString(fileContent);
			user.setImg(encodedString);
		}
		}
		return user;

	}
	@WebMethod
	public Usuario consultarPorEmail(String email) {
		Usuario user = controller.consultarEmail(email);	
		return user;
	}

	@WebMethod

	public List<Canal> listCanalesSeguidos(String nickname) {
		List<Canal> seguidos = controller.listCanalesSeguidos(nickname);
		return seguidos;

	}
	@WebMethod
	public List<Usuario> listUsuariosSeguidores(String nickname) {
		List<Usuario> seguidores = controller.listUsuariosSeguidores(nickname);
		return seguidores;

	}
	@WebMethod
	public void setHistorial(Video video, Usuario user) {
		System.out.println(user);
		Historial historial = new Historial(user, video);
		controller.setHistorial(historial);
	}
	@WebMethod
	public List<Historial> getHistorial(String nickname) {
		List<Historial> historial = controller.getHistorial(nickname);
		return historial;
	}
	
	@WebMethod
	public void setPassword(Usuario user, String password) {
		user.setPassword(password);
	}

}