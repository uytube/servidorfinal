package servicios;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.Endpoint;

import uytube.App;
import uytube.VideoController.VideoController;
import uytube.models.Categoria;
import uytube.models.Video;

@WebService
@SOAPBinding(style = Style.RPC, parameterStyle = ParameterStyle.WRAPPED)
public class VideoEndpoint {

	private Endpoint endpoint;
	private VideoController controllerVideo;

	public VideoEndpoint() {

		controllerVideo = new VideoController();
	}

	@WebMethod(exclude = true)
	public void publicar() {
		String ip = "localhost"; //default
//		try {
//			ip = App.LecuturaIP();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		String ipTotal = "http://"+ip+":9876/video";
		System.out.println("LA IP TOTAL ES: "+ipTotal);
		endpoint = Endpoint.publish(ipTotal, this);
	}

	@WebMethod(exclude = true)
	public Endpoint getEndpoint() {
		return endpoint;
	}

	// DatosDelVideo
	@WebMethod
	public Video DatosDelVideo(int id) {
		return this.controllerVideo.consultaVideoPorID(id);
	}

	// Lista de videos del usuario
	@WebMethod
	public Video[] VideosUsuario(String nick) {
		List<Video> videos = this.controllerVideo.listaVideosUsuario(nick);
		Video[] retornar = new Video[videos.size()];
		for (int i = 0; i < retornar.length; i++) {
			retornar[i] = videos.get(i);
		}
		return retornar;
	}

	// Obtener videos usuario
	@WebMethod
	public Video[] ObtenerVideosUsuario(String nick) {
		List<Video> videos = this.controllerVideo.obtenerVideosUsuario(nick);
		Video[] retornar = new Video[videos.size()];
		for (int i = 0; i < retornar.length; i++) {
			retornar[i] = videos.get(i);
		}
		return retornar;
		// return this.controllerVideo.obtenerVideosUsuario(nick);
	}
	
	@WebMethod
	public void modificarVideo(Video vid) {
		this.controllerVideo.modificarVideo(vid);
	}
	
	@WebMethod
	public void subirVideoYT (Video vid, String usr, String cate) {
		this.controllerVideo.altaVideo(vid, usr, cate);				
	}
	
	@WebMethod
	public Video[] videoPorCategoria(Categoria cat){
		List<Video> videos = this.controllerVideo.videoPorCategoria(cat);
		Video[] retornar = new Video[videos.size()];
		for (int i = 0; i < retornar.length; i++) {
			System.out.println(videos.get(i));
			retornar[i] = videos.get(i);
		}
		System.out.println("-------");
		System.out.print("-------");
		System.out.println("-------");
		
		for (Video v : retornar) {
			System.out.println(".."+v.getNombre());
		}
		System.out.print("-------");
		System.out.println("-------");
		System.out.println("-------");
		return retornar;
	}
	

}
