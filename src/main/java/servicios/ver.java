package servicios;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.Endpoint;

import uytube.models.Canal;
import uytube.models.Categoria;
import uytube.models.Comentario;
import uytube.models.Lista;
import uytube.models.Usuario;
import uytube.models.ValoracionVideo;
import uytube.models.Video;
import uytube.App;
import uytube.CategoriaController.CategoriaController;
import uytube.ComentarioController.ComentarioController;
import uytube.ListaController.ListaController;
import uytube.UsuarioController.UsuarioController;
import uytube.ValoracionController.ValoracionController;
import uytube.VideoController.*;

@WebService
@SOAPBinding(style = Style.RPC, parameterStyle = ParameterStyle.WRAPPED)
public class ver {

	private Endpoint endpoint;
	private CategoriaController controllerCategoria;
	private VideoController controllerVideo;
	private UsuarioController controllerUser;
	private ValoracionController controllerValoracion;
	private ComentarioController controllerComentario;
	private ListaController controllerLista;

	public ver() {

		controllerCategoria = new CategoriaController();
		controllerVideo = new VideoController();
		controllerUser = new UsuarioController();
		controllerValoracion = new ValoracionController();
		controllerComentario = new ComentarioController();
		controllerLista = new ListaController();
	}

	@WebMethod(exclude = true)
	public void publicar() {
		String ip = "localhost"; //default
//		try {
//			ip = App.LecuturaIP();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		String ipTotal = "http://"+ip+":9875/ver";
		System.out.println("LA IP TOTAL ES: "+ipTotal);
		endpoint = Endpoint.publish(ipTotal, this);
	}

	@WebMethod(exclude = true)
	public Endpoint getEndpoint() {
		return endpoint;
	}

	// Lista de categorias
	@WebMethod
	public Categoria[] ListaDeCategorias() {
		List<Categoria> categorias = this.controllerCategoria.listarCategorias();
		Categoria[] retornar = new Categoria[categorias.size()];
		for (int i = 0; i < categorias.size(); i++) {
			retornar[i] = categorias.get(i);
		}

		return retornar;
	}

	// DatosDelVideo
	@WebMethod
	public Video DatosDelVideo(int id) {
		return this.controllerVideo.consultaVideoPorID(id);
	}

	// CanalesSeguidos
	@WebMethod
	public Canal[] CanalesSeguidos(String nick) {
		List<Canal> canales = this.controllerUser.listCanalesSeguidos(nick);
		Canal[] retornar = new Canal[canales.size()];
		for (int i = 0; i < retornar.length; i++) {
			retornar[i] = canales.get(i);
		}
		return retornar;
	}

	// ValoracionesVideo
	@WebMethod
	public ValoracionVideo[] ValoracionesVideo(int id_video) {
		List<ValoracionVideo> valoraciones = this.controllerValoracion.listaValoracionesVideo(id_video);
		ValoracionVideo[] retornar = new ValoracionVideo[valoraciones.size()];
		for (int i = 0; i < retornar.length; i++) {
			retornar[i] = valoraciones.get(i);
		}

		return retornar;
	}

	// TraerValoracion
	@WebMethod
	public ValoracionVideo DevolverValoracionVideo(int id_video, String nick) {
		return this.controllerValoracion.traerValoracion(id_video, nick);
	}

	// Lista de videos del usuario
	@WebMethod
	public Video[] VideosUsuario(String nick) {
		List<Video> videos = this.controllerVideo.listaVideosUsuario(nick);
		Video[] retornar = new Video[videos.size()];
		for (int i = 0; i < retornar.length; i++) {
			retornar[i] = videos.get(i);
		}
		return retornar;
	}

	// Lista de valoraciones de un video
	@WebMethod
	public ValoracionVideo[] ValoracionesDeVideo(int id) {
		List<ValoracionVideo> valoraciones = this.controllerValoracion.listaValoracionesVideo(id);
		ValoracionVideo[] retornar = new ValoracionVideo[valoraciones.size()];
		for (int i = 0; i < retornar.length; i++) {
			retornar[i] = valoraciones.get(i);
		}

		return retornar;
		// return (ArrayList)this.controllerValoracion.listaValoracionesVideo(id);
	}

	// Obtener videos usuario
	@WebMethod
	public Video[] ObtenerVideosUsuario(String nick) {
		List<Video> videos = this.controllerVideo.obtenerVideosUsuario(nick);
		Video[] retornar = new Video[videos.size()];
		for (int i = 0; i < retornar.length; i++) {
			retornar[i] = videos.get(i);
		}
		return retornar;
		// return this.controllerVideo.obtenerVideosUsuario(nick);
	}

	// Comentarios de un video
	@WebMethod
	public Comentario[] ComentariosDeUnVideo(String nombre_video) {
		List<Comentario> comentarios = this.controllerComentario.listarComentarios(nombre_video);
		Comentario[] retornar = new Comentario[comentarios.size()];
		for (int i = 0; i < retornar.length; i++) {
			retornar[i] = comentarios.get(i);
		}
		return retornar;
		// return (ArrayList)this.controllerComentario.listarComentarios(nombre_video);
	}

	// Seguir usuario
	@WebMethod
	public void postSeguirUsuario(String usuario_logueado, String nickname) {
		controllerUser.seguirUsuario(usuario_logueado, nickname);
	}

	// Dejar de seguir usuario ( canal )
	@WebMethod
	public void postDejarSeguirUsuario(String usuario_logueado, String canal_nombre) {
		controllerUser.dejarDeSeguir(usuario_logueado, canal_nombre);
	}

	// Valorar Video
	@WebMethod
	public String postValorarVideo(int id_video, String user, int nueva_valoracion) {

		ValoracionVideo valoracion;
		try {
			valoracion = controllerValoracion.traerValoracion(id_video, user);
			// En este punto, si el usuairo no habia valorado antes el video, saltara una
			// excepcion
			valoracion.setValoracion(nueva_valoracion);

			System.out.println("Usuario de valoracion: " + valoracion.getUsuario().getNickname());

			controllerValoracion.valorarVideo(valoracion);

			return "WS: Valoracion modificada";
		} catch (Exception e) {
			System.out.println("Except valoration!");
			// Nueva valoracion
			Usuario usuario = controllerUser.consultarUsuario(user);
			valoracion = new ValoracionVideo();
			valoracion.setVideo(controllerVideo.consultaVideoPorID(id_video));
			valoracion.setUsuario(usuario);
			valoracion.setValoracion(nueva_valoracion);
			controllerValoracion.valorarVideo(valoracion);

			return "WS: Nueva valoracion";
		}

	}

	// Comentar Video o comentario
	@WebMethod
	public String postComentarioVideo(int id_video, String comentario, int id_comentario, String user) {

		Usuario usuario = this.controllerUser.consultarUsuario(user);

		// Si es respuesta
		if (id_comentario != -1) {
			Comentario respuesta = new Comentario();

			respuesta.setComentario(comentario);
			respuesta.setFecha(new Date());
			respuesta.setVid(null);

			respuesta.setUsuario(usuario);

			controllerComentario.AgregarRespuesta((long) id_comentario, respuesta);
			return "WS: Respuesta lista";

		} else {
			// Si es comentario al video

			Video vid = controllerVideo.consultaVideoPorID(id_video);
			Comentario com = new Comentario();

			com.setComentario(comentario);
			com.setFecha(new Date());
			com.setVid(vid);

			com.setUsuario(usuario);

			controllerComentario.AgregarComentario(com);

			return "WS: Comentario listo";
		}
	}

	//Agregar video a la lista de reproduccion
	@WebMethod
	public String agregarVideo(int id_video, int id_lista) {

		try {
			this.controllerLista.agregarVideo(id_video, id_lista);
			return "Video agregado a la lista";
		} catch (Exception e) {
			return "Video ya esta en la lista";
		}
	}
	
	//Quitar video de la lsita de reproduccion
	@WebMethod
	public String quitarVideo(int id_video, int id_lista) {

		try {
			this.controllerLista.quitarVideo(id_video, id_lista);
			return "Video quitado de la lista";
		} catch (Exception e) {
			return "Video no esta en la lista";
		}
	}
	
	//Crear lista de repro.
	@WebMethod
	public void postCrearLista(String nombre_lista,String categoria,boolean privado,String usuario) {
		this.controllerLista.crearLista(nombre_lista, "Sin Categoria",usuario, privado, false);	
	}
	
	//Listar listas del usuario logueado
	@WebMethod
	public Lista[] listarListas(String nick) {
		List<Lista> listas = this.controllerLista.listarListas(nick);
		Lista[] retornar = new Lista[listas.size()];
		for (int i = 0; i < retornar.length; i++) {
			retornar[i] = listas.get(i);
		}
		return retornar;
	}

}