package servicios;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.Endpoint;

import uytube.models.Canal;
import uytube.App;
import uytube.CanalController.CanalController;

@WebService
@SOAPBinding(style = Style.DOCUMENT, parameterStyle = ParameterStyle.WRAPPED)
public class WslCanal {

	private Endpoint endpoint;
	private CanalController controller;
	public WslCanal() {
		controller = new CanalController();
	}
	
	@WebMethod(exclude = true)
	public void publicar() {
		String ip = "localhost"; //default
//		try {
//			ip = App.LecuturaIP();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		String ipTotal = "http://"+ip+":9874/canal";
		System.out.println("LA IP TOTAL ES: "+ipTotal);
		endpoint = Endpoint.publish(ipTotal, this);
	}
	
	@WebMethod(exclude = true)
	public Endpoint getEndpoint() {
		return endpoint;
	}
	
	/*
	 * Ejemplo devolviendo un objeto
	 * */

	@WebMethod
	public Canal obtenerCanalUsuario(String nickname) {
		System.out.println(nickname);
		Canal canal = controller.obtenerCanalUsuario(nickname);		
		return canal;
	}
	@WebMethod
	public void actualizarCanal(Canal canal) {
		controller.actualizarCanal(canal);		
	}
	
	@WebMethod
	public List<Canal> searchCanales(String search,String alfabeticamente) {
		List<Canal> canales = controller.searchCanales(search, alfabeticamente);	
		return canales;
	}


}