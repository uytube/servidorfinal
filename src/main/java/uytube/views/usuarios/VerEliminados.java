package uytube.views.usuarios;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import uytube.UsuarioController.UsuarioController;
import uytube.models.Usuario;
import uytube.views.Frame;
import uytube.views.Inicio;
import uytube.views.usuarios.consultar.Consultar;
import uytube.views.usuarios.consultar.ConsultarEliminados;

public class VerEliminados extends JPanel {
	private JTable table;

	/**
	 * Create the panel.
	 */
	private String [] nombreColumnas = {"Nombre","Apellido","Nickname","Correo","F.Nac"};
	private String [][] datos ;
	private JTable table_1;
	private JFrame frame;
	private Usuario user;
	public VerEliminados() {
		setLayout(null);
		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Inicio inicio = new Inicio();
				Frame.frame.setContentPane(inicio);
				Frame.frame.validate();			
			}
		});
		btnVolver.setBounds(10, 431, 368, 23);
		add(btnVolver);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 100, 780, 313);
		add(scrollPane);
		
		UsuarioController controller = new UsuarioController();
		List<Usuario> usuarios = controller.usuariosEliminados();
		DefaultTableModel  tablemodel = new DefaultTableModel(nombreColumnas, 0);
		table = new JTable();
		for(Usuario u:usuarios) {
			tablemodel.addRow(
					new Object[] {
							u.getNombre(),
							u.getApellido(),
							u.getNickname(),
							u.getCorreo(),
							u.getFnacimiento()
					}
			);
		}
		
		table.setModel(tablemodel);
		scrollPane.setViewportView(table);
		
		JButton btnEditarUsuario = new JButton("Consultar usuario");
		btnEditarUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ConsultarEliminados consultar = new ConsultarEliminados(user);
				Frame.frame.setContentPane(consultar);
				Frame.frame.revalidate();

			}
		});
		btnEditarUsuario.setBounds(408, 432, 382, 23);
		add(btnEditarUsuario);
		
		JLabel label = new JLabel("USUARIOS DADOS DE BAJA");
		label.setBounds(10, 51, 196, 14);
		add(label);
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				user = usuarios.get(e.getFirstIndex());
				btnEditarUsuario.setVisible(true);
			}
		});
		
		

	}
}
