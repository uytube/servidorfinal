package uytube.CanalController;

import java.util.ArrayList;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.hibernate.Session;
import org.hibernate.Transaction;

import uytube.CanalController.ICanal;
import uytube.models.Canal;
import uytube.models.HibernateUtil;
import uytube.models.Usuario;
import uytube.models.manager.Manager;

public class CanalController implements ICanal {

	// Variables de conexion
	private Manager mng;
	private Session session;
	private Transaction transaction;
	private static EntityManager manager;
	private static EntityManagerFactory emf;

	public CanalController() {
		mng = Manager.getInstance();
	}

	public String obtenerCanalUsuarioBaja(String nickname) {
		String canal = (String) mng.getSessionManager()
				.createNativeQuery("Select descripcion From Canal where usuario_nickname = :nickname AND deleted = 1")
				.setParameter("nickname", nickname).getSingleResult();
		mng.closeSession();
		return canal;
	}
	public ArrayList<Canal> searchCanales(String search, String alfabeticamente) {
		String query = "";
		if(alfabeticamente!=null) {
			query+="order by ";
		}
		if(alfabeticamente.equals("Z-A")) {
			query+="nombre desc";
		}else {
			query+="nombre asc";
		}
		ArrayList<Canal> c = (ArrayList<Canal>)mng.getSessionManager().
			createQuery("From Canal where privacidad=0 and nombre like :search "+query).setParameter("search", "%"+search+"%").getResultList();			
		mng.closeSession();
		return c;
			
	}

	public Canal obtenerCanalUsuario(String nickname) {
		System.out.println(nickname);
		Canal canal = (Canal) mng.getSessionManager().createQuery("From Canal where usuario_nickname = :nickname")
				.setParameter("nickname", nickname).getSingleResult();
		mng.closeSession();
		return canal;

	}

	public void actualizarCanal(Canal canal) {
		this.session = null;
		this.transaction = null;
		session = HibernateUtil.getSessionFactory().openSession();
		try {
			transaction = session.beginTransaction();
			if (!transaction.isActive())
				transaction.begin();
			session.saveOrUpdate("Canal", canal);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
}
