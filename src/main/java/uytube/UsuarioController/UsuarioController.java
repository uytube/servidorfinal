package uytube.UsuarioController;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JOptionPane;
import org.hibernate.Transaction;
import org.hibernate.Session;
import uytube.models.Usuario;
import uytube.models.ValoracionVideo;
import uytube.models.Video;
import uytube.models.manager.Manager;
import uytube.CanalController.CanalController;
import uytube.CanalController.ICanal;
import uytube.ComentarioController.ComentarioController;
import uytube.ComentarioController.IComentario;
import uytube.ListaController.ILista;
import uytube.ListaController.ListaController;
import uytube.ValoracionController.IValoracion;
import uytube.ValoracionController.ValoracionController;
import uytube.VideoController.IVideo;
import uytube.VideoController.VideoController;
import uytube.models.Canal;
import uytube.models.Comentario;
import uytube.models.HibernateUtil;
import uytube.models.Historial;
import uytube.models.Lista;
public class UsuarioController implements IUsuario{
	
	private Manager mng;
	
	private Session session;
	private Transaction transaction;
	private static EntityManager manager;
	private static EntityManagerFactory emf;
	
	public UsuarioController() {
		mng = Manager.getInstance();
	}
	
	public void crearUsuario(Usuario usuario, Canal canal) {
		this.session = null;
		this.transaction = null;
		
		//Listas default a este canal
		usuario.setDeleted(false);
		canal.setDeleted(false);
		
	    try {
	        session = HibernateUtil.getSessionFactory().openSession();
	        transaction = session.beginTransaction();
	        if(!transaction.isActive())
	        	transaction.begin();
	        session.saveOrUpdate("Canal", canal);
	        transaction.commit();
	      } catch (Exception e) {
	        if (transaction != null) {
	          transaction.rollback();
	        }
	        e.printStackTrace();
	      } finally {
	        if (session != null) {
	          session.close();
	        }
	      }	
		ILista controllerLista = new ListaController();
		controllerLista.crearLista("Escuchar mas tarde", "Sin Categoria", usuario.getNickname(), true, true);
		controllerLista.crearLista("Deporte total", "Sin Categoria", usuario.getNickname(), true, true);
		controllerLista.crearLista("Novedades generales", "Sin Categoria", usuario.getNickname(), true, true);
		controllerLista.asignarListasDefault(canal.getNombre());

	}

	public ArrayList<Usuario> listaUsuarios() {
		this.session = null;
		ArrayList<Usuario> usuarios = null;
	    try {
	        session = HibernateUtil.getSessionFactory().openSession();
	        usuarios = (ArrayList<Usuario>)session.createQuery("From Usuario").getResultList();
	    } catch (Exception e) {
	        e.printStackTrace();
	      } finally {
	        if (session != null) {
	          session.close();
	        }
	      }		
		return usuarios;
	}
	public Usuario consultarUsuario(String nickname) {
		this.session = null;
		System.out.println(nickname);
		Usuario usuario = null;
	    try {
	    	this.session = HibernateUtil.getSessionFactory().openSession();
	        usuario = (Usuario)session.createQuery("From Usuario where nickname=:nickname").setParameter("nickname", nickname).getSingleResult();
	    } catch (Exception e) {
	      } finally {
	        if (this.session != null) {
	          this.session.close();
	        }
	      }		
		return usuario;
	}	
	public Usuario consultarEmail(String correo) {
		this.session = null;
		Usuario usuario = null;
	    try {
	        session = HibernateUtil.getSessionFactory().openSession();
	        usuario = (Usuario)session.createQuery("From Usuario where correo=:correo").setParameter("correo", correo).getSingleResult();
	    } catch (Exception e) {
	      } finally {
	        if (session != null) {
	          session.close();
	        }
	      }		
		return usuario;
	}	
	public Usuario login(String nickname,String password) {
		Usuario usuario = null;
		try {
			usuario = (Usuario)mng.getSessionManager().createQuery("From Usuario where nickname=:nickname and password=:password").setParameter("nickname", nickname).setParameter("password", password).getSingleResult();
			mng.closeSession();			
		} catch (Exception e) {
			
		}
		return usuario;
	}	
	public void modificarUsuario(Usuario usuario) {
		this.session = null;
		this.transaction = null;
		session = HibernateUtil.getSessionFactory().openSession();
		try {
	        transaction = session.beginTransaction();
	        if(!transaction.isActive())
	        	transaction.begin();
	        System.out.println(usuario.getNickname());
	        System.out.println(usuario.getNombre());
	        session.saveOrUpdate(usuario);
	        transaction.commit();
	      } catch (Exception e) {
	    	e.getStackTrace();
	        if (transaction != null) {
	          transaction.rollback();
	        }
	      } finally {
	        if (session != null) {
	          session.close();
	        }
	      }		
	}

	public void seguirUsuario(String nickUser,String nameCanal) {
		// TODO Auto-generated method stub	
		
		//Usuario sigue canales
		
		//Obtener el usuario y luego el canal
		
		Usuario user = (Usuario)mng.getSessionManager().createQuery("From Usuario where nickname = :nameUser").setParameter("nameUser", nickUser).getSingleResult();
		mng.closeSession();
		
		Canal canal = (Canal)mng.getSessionManager().createQuery("From Canal where usuario_nickname = :nombreCanal").setParameter("nombreCanal", nameCanal).getSingleResult();
		mng.closeSession();
		
		user.addCanal(canal);
		
		mng.startTransaction("Usuario", user);
		
		System.out.println(user.getNickname()+" sigue a "+canal.getNombre());
		
	}
	
	public List<Canal> listCanalesSeguidos(String nick) {
		
		Usuario user = (Usuario) mng.getSessionManager().createQuery("From Usuario where nickname = :nick").setParameter("nick",nick).getSingleResult();
		
		return user.getCanalesSeguidos();
		
	}
	public List<Usuario> listUsuariosSeguidores(String nickname){
		List<Usuario> users = (List<Usuario>) mng.getSessionManager().
								createQuery("select u From Usuario as u inner join u.canalesSeguidos as canalesSeguidos where canalesSeguidos.usuario.nickname = :nick").
								setParameter("nick",nickname).getResultList();
		return users;	
		
	}
	@SuppressWarnings("unchecked")
	public void darDeBaja(String nickname) {

		IValoracion ivaloracion = new ValoracionController();
		ICanal icanal = new CanalController();
		ILista ilista = new ListaController();
		IComentario icomentario = new ComentarioController();

		/*
		 * Dar de baja a: usuario su canal sus listas sus videos
		 * 
		 */

		Canal canal = icanal.obtenerCanalUsuario(nickname);

		// Eliminar valoraciones

		List<ValoracionVideo> valoraciones = (List<ValoracionVideo>) mng.getSessionManager()
				.createQuery("From ValoracionVideo Where usuario.nickname = :nick").setParameter("nick", nickname)
				.getResultList();
		mng.closeSession();
		Iterator<ValoracionVideo> iv = valoraciones.iterator();
		while (iv.hasNext()) {
			System.out.println(":: Removiendo valoraciones...");
			ValoracionVideo vv = (ValoracionVideo) iv.next();
			System.out.println(":: Valoracion -> "+vv.getId());
			mng.deleteValoracionesSQL(nickname);
		}

		 

		List<Comentario> comentarios = (List<Comentario>) mng.getSessionManager()
				.createQuery("From Comentario where usuario.nickname = :nick").setParameter("nick", nickname)
				.getResultList();
		mng.closeSession();

		Iterator<Comentario> ic = comentarios.iterator();
		while (ic.hasNext()) {
			System.out.println(":: Removiendo comentarios...");
			Comentario com = (Comentario) ic.next();
			
			// Remover comentarios
			List<Comentario> respuestas = com.getRespuestas();
			respuestas.clear();
			mng.startTransaction("Comentario", com);
			
			mng.deleteEntity(com,"comentario");
			
			/*
			mng.getSessionManager().createNativeQuery("DELETE FROM Comentario_Comentario WHERE respuestas_id = :idcom").
			setParameter("idcom", com.getId()).executeUpdate();
			
			mng.closeSession();
			
			mng.getSessionManager().createQuery("DELETE FROM Comentario WHERE id = :idcom").
			setParameter("idcom", com.getId()).executeUpdate();
			
			mng.closeSession();
*/
		}

		// Eliminar Seguidos
		List<Canal> canalesSeguidos = this.listCanalesSeguidos(nickname);

		for (Canal c : canalesSeguidos) {
			this.dejarDeSeguir(nickname, c.getNombre());
		}

		// Eliminar seguidores

		List<Usuario> seguidores = this.listUsuariosSeguidores(nickname);
		for (Usuario seguidor : seguidores) {
			seguidor.dejarDeSeguir(canal.getNombre());
		}

		// Videos
		IVideo ivideo = new VideoController();

		List<Video> videos = ivideo.obtenerVideosUsuario(nickname);
		for (Video v : videos) {
			v.setDeleted(true);
			mng.startTransaction("Video", v);
		}

		// Lista y video

		List<Lista> listas = ilista.listarListas(nickname);
		for (Lista l : listas) {
			l.setDeleted(true);
			mng.startTransaction("Lista", l);
		}

		// Canal y usuario

		canal.getUsuario().setDeleted(true);
		canal.setDeleted(true);

		mng.startTransaction("Canal", canal);

		// Eliminar valoraciones
		// comentarios
		// seguidos y seguidores
	}
	
	public List<Usuario> usuariosEliminados() {
		List<Usuario> retornar = (List<Usuario>)mng.getSessionManager().createNativeQuery("Select * From Usuario WHERE deleted = 1",Usuario.class).getResultList();
		mng.closeSession();
		return retornar;
	}


	public List<Historial> getHistorial(String nickname){
		List<Historial> historial = (List<Historial>) mng.getSessionManager().createQuery("From Historial where usuario.nickname = :nick group by video_id order by fecha asc").setParameter("nick",nickname).getResultList();
		return historial;			
	}

	public void setHistorial(Historial historial) {
		this.session = null;
		this.transaction = null;
		session = HibernateUtil.getSessionFactory().openSession();
		try {
	        transaction = session.beginTransaction();
	        if(!transaction.isActive())
	        	transaction.begin();
	        session.saveOrUpdate(historial);
	        transaction.commit();
	      } catch (Exception e) {
	    	e.printStackTrace();
	        if (transaction != null) {
	          transaction.rollback();
	        }
	      } finally {
	        if (session != null) {
	          session.close();
	        }
	      }		
	}

	
	private Usuario getUser(String nick) {
		emf = Persistence.createEntityManagerFactory("uytube");
		manager = this.emf.createEntityManager();
		Usuario user = (Usuario)manager
		.createQuery("From Usuario Where nickname = :nick")
		.setParameter("nick", nick).getSingleResult();
		
		return user;
	}
	public void dejarDeSeguir(String nickUser,String nameCanal) {
		// TODO Auto-generated method stub
		Usuario user = (Usuario)mng.getSessionManager().createQuery("From Usuario where nickname = :nameUser").setParameter("nameUser", nickUser).getSingleResult();
		mng.closeSession();
		
		Canal canal = (Canal)mng.getSessionManager().createQuery("From Canal where nombre = :nombreCanal").setParameter("nombreCanal", nameCanal).getSingleResult();
		mng.closeSession();
		
		user.dejarDeSeguir(nameCanal);
		
		mng.startTransaction("Usuario", user);
		
		
	}

	public void listarSeguidores(String nickUser) {
		
		Usuario user = (Usuario)mng.getSessionManager().createQuery("From Usuario where nickname = :nameUser").setParameter("nameUser", nickUser).getSingleResult();
		mng.closeSession();
		
		System.out.println(" :: Siguiendo :: ");
		for(Canal c:user.getCanalesSeguidos()) {
			System.out.println(c.getNombre());
		}
		
	}
	
	public void modificarDatos() {
		// TODO Auto-generated method stub
		
	}

}