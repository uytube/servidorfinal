package uytube.models.manager;

import org.hibernate.Session;
import org.hibernate.Transaction;

import uytube.models.Comentario;
import uytube.models.HibernateUtil;

public class Manager {

	/*
	 * Manager para ahorrar lineas de codigo al hacer la transferencia
	 */

	private Session session;
	private Transaction transaction;
	private static Manager instance = null;

	private Manager() {
		this.instance = this;
		this.session = null;
		this.transaction = null;
	}

	public static Manager getInstance() {
		if (instance == null) {
			System.out.println("Creating new Manager database");
			return new Manager();
		}
		System.out.println("Manager already created");
		return instance;
	}

	public void startTransaction(String columna, Object obj) {
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			if (!transaction.isActive())
				transaction.begin();
			session.saveOrUpdate(columna, obj);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void deleteEntity(Object obj, String type) {
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			if (!transaction.isActive())
				transaction.begin();
			session.remove(obj);
			transaction.commit();
			System.out.println("::: Deleted OK");
		} catch (Exception e) {

			System.out.println("::: Comentario respuesta a otro");
			if (transaction != null) {
				transaction.rollback();
			}

			if (type.equals("comentario")) {
				try {
					transaction = session.beginTransaction();
					if (!transaction.isActive())
						transaction.begin();
					session.createNativeQuery("DELETE FROM Comentario_Comentario WHERE respuestas_id = :idcom")
							.setParameter("idcom", ((Comentario) obj).getId()).executeUpdate();
					transaction.commit();

				} catch (Exception ee) {
					ee.printStackTrace();
				}

			}else {
				//valoracion
				//session.createNativeQuery("DELETE FROM ValoracionVideo where usuario.nickname = :nick").
				//setParameter("nick", nickname);
			}
			System.out.println(":::::::");
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}

			// throw new Exception("Se debe eliminar la respuesta del comentario ajeno");
		}
	}
	
	public void deleteValoracionesSQL(String nick) {
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			if (!transaction.isActive())
				transaction.begin();
			// Consulta
			session.createNativeQuery("DELETE FROM ValoracionVideo where usuario_nickname = :nick").
			setParameter("nick", nick).executeUpdate();
			////
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	// Cerrar session
	public void closeSession() {
		session.close();
	}

	// Recordar cerrar session luego de pedirla y usarla
	public Session getSessionManager() {
		session = HibernateUtil.getSessionFactory().openSession();
		return this.session;
	}

}
