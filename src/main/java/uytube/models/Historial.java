package uytube.models;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "Historial")
public class Historial {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name ="id", insertable=false, updatable=false)
	private int id;

	
	@Column(name = "fecha")
	private Date fecha;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Usuario usuario;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Video video;
	
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void setVideo(Video video) {
		this.video = video;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}
	
	public Video getVideo() {
		return this.video;
	}
	public Historial() {
		
	}
	public Historial(Usuario user,Video video) {
		super();
		this.usuario = user;
		this.video = video;
		this.fecha = new Date();
	}

	
}
